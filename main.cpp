#include <iostream>
#include <cstdlib>
#include <cmath>
#include <string.h>

using namespace std;

void nextChar();

bool toNext(int charToNext);

double parseNext();

double parseTerm();

double parseExpression();

double parse();

void normalizeExpression();

bool checkNext(char nextSymbol, int from);


class CalculationException : public std::exception {
    char *message;
public:
    explicit CalculationException(char *message) {
        this->message = message;
    }

    const char *what() const throw() override {
        return message;
    }
};


string expression;

int position = -1, charSymbol = 0;

int main() {
    expression = "0";
    while (1) {
        cout << " \nInput an expression \n\n";
        getline(cin, expression);
        try {
            cout << "\n" << parse();
        } catch (CalculationException ex) {
            cout << "\n" << ex.what() << "\n";
        }
        position = -1;
        charSymbol = 0;
    }
    return 0;
}


void nextChar() {
    charSymbol = ++position < expression.size() ? expression[position] : -1;
}

bool toNext(int charToNext) {
    if (charSymbol == charToNext) {
        nextChar();
        return true;
    }
    return false;
}

double parseNext() {
    //Последующее является положительным
    if (toNext('+'))
        return parseNext();
    //Последующее является отрицательным
    if (toNext('-'))
        return -parseNext();

    double x;
    int startPosition = position;
    if (toNext('(')) {
        x = parseExpression();
        toNext(')');
    } else if ((charSymbol >= '0' && charSymbol <= '9') || charSymbol == '.') {
        while ((charSymbol >= '0' && charSymbol <= '9') || charSymbol == '.') nextChar();
        x = atof(expression.substr(startPosition, position).c_str());
    } else if (charSymbol >= 'a' && charSymbol <= 'z') {
        //Если это функция
        int i = 0;
        while ((charSymbol >= 'a' && charSymbol <= 'z')) {
            i++;
            nextChar();
        }
        string func = expression.substr(startPosition, i);
        x = parseNext();
        if (func == "sqrt") {
            x = sqrt(x);
        } else if (func == "sin") {
            x = sin(x);
        } else if (func == "tan") {
            x = tan(x);
        } else if (func == "cos") {
            x = cos(x);
        } else {
            throw CalculationException(strdup(("Unknown function " + func).c_str()));
        }
    } else {
        string message = "Unsupported character";
        throw CalculationException(strdup((message).c_str()));
    }

    if (toNext('^')) x = pow(x, parseNext());

    return x;
}

double parseTerm() {
    double x = parseNext();
    while (1) {
        if (toNext('*'))
            x *= parseNext();
        else if (toNext('/'))
            x /= parseNext();
        else
            return x;
    }
}

double parseExpression() {
    double x = parseTerm();
    while (1) {
        if (toNext('+'))
            x += parseTerm();
        else if (toNext('-'))
            x -= parseTerm();
        else
            return x;
    }
}

void normalizeExpression() {
    for (int i = 0; i < expression.size() - 1; i++) {
        if (expression[i] == ' ') {
            expression.erase(i, 1);
        }
        if (checkNext('(', i + 1) && (expression[i] >= '0' && expression[i] <= '9')) {
            expression.insert(i + 1, 1, '*');
        }
    }
}

bool checkNext(char nextSymbol, int from) {
    while (expression[from] == ' ') from++;
    return nextSymbol == expression[from];
}

double parse() {
    normalizeExpression();
    nextChar();
    double x = parseExpression();
    if (position < expression.length()) {
        string message = "Failed to calculate expression with unknown error. Unexpected calculation error at position ";
        throw CalculationException(strdup((message).c_str()));
    }
    return x;
}

